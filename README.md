```
  __   __        _ __  _ __ 
 / _| / _| __ _ | '_ \| '_ \
|  _||  _|/ _` || .__/| .__/
|_|  |_|  \__/_||_|   |_|   
```
# ffapp
ffapp is a simple tool for creating and managing Firefox web apps. It works by creating a Firefox profile, applying some custom CSS/JS, and creating a .desktop file to open the app from your application menu.

This is similar in spirit to [PWAsForFirefox](https://github.com/filips123/PWAsForFirefox), except I found that to only work on true PWAs where you can provide it a manifest file to build the app.
ffapp needs only a root URL and an icon URL to work.
It is a single Python file with almost no dependencies.

## How to run
```
./ffapp.py
```

## How to install
With git
```
git clone git@gitlab.com:neilsimp1/ffapp.git
cd ffapp
```
Also available in the AUR: https://aur.archlinux.org/packages/ffapp

## Known Issues
1. Currently only able to fetch icons from a Web URL. Will add support for local file paths sometime.
2. ffapp can detect what icon to use on plenty of webapps, but it's not foolproof. You may end up needing to specify a URL.
3. The URLs you provide must be a direct link, and anything that 300s might not work right.
