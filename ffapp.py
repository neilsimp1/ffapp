#!/usr/bin/env python

import glob
import os
from os import path
import re
import requests
import shutil
import subprocess
import urllib.parse

APP_PATH = path.join(os.environ['HOME'], ".local/share/applications")
ICON_PATH = path.join(os.environ['HOME'], ".local/share/icons")
MOZ_PATH = path.join(os.environ['HOME'], ".mozilla/firefox")

class AppConfig(object):
	def __init__(self, url, name, icon_url, css_option, is_mobile, show_titlebar):
		self.url = url
		self.id = "ffapp-" + name.replace(' ', '').lower()
		self.name = name
		self.icon_url = icon_url
		self.css_config = CssConfig(css_option, is_mobile)
		self.show_titlebar = show_titlebar

class CssConfig(object):
	def __init__(self, css_option, is_mobile):
		self.css_option = css_option
		self.is_mobile = is_mobile

def get_css(css_config):
	css = ""
	if css_config.css_option == '1':
		css = """\
#TabsToolbar {visibility: collapse;}
#navigator-toolbox {visibility: collapse;}
"""
	elif css_config.css_option == '2':
		css = """\
#TabsToolbar {visibility: collapse;}
#nav-bar-customization-target toolbarbutton:not(#back-button):not(#forward-button), #nav-bar-customization-target toolbaritem {visibility: collapse;}
#PanelUI-button{visibility: collapse;}
"""
	if css_config.is_mobile:
		is_ff_esr = get_is_ff_esr()
		css_file_path = path.join(path.dirname(path.realpath(__file__)), "firefox-esr-mobile.css" if is_ff_esr else "firefox-mobile.css")
		with open(css_file_path, 'r') as file:
			css += file.read()

	return css

def get_js(show_titlebar):
	return f"""\
user_pref('toolkit.legacyUserProfileCustomizations.stylesheets', true);
user_pref('browser.aboutConfig.showWarning', false);
user_pref('browser.link.open_newwindow', 2);
user_pref('browser.tabs.closeWindowWithLastTab', true);
user_pref('dom.security.https_only_mode', false);
user_pref('browser.sessionstore.resume_from_crash', false);
user_pref('browser.tabs.inTitlebar', { 0 if show_titlebar else 1 });
"""

def get_is_ff_esr():
	output = subprocess.check_output(["firefox", "--version"], text=True)
	return "esr" in output.lower()

def create_ff_profile(app_id):
	subprocess.run(["firefox", "--CreateProfile", f"{app_id} {MOZ_PATH}/{app_id}"])

def get_ff_profile_path(app_config):
	profile_path = [x for x in os.listdir(MOZ_PATH) if path.isdir(path.join(MOZ_PATH, x)) if app_config.id in x]
	if not profile_path:
		raise Exception(f"Cannot find profile: {app_config.name}")
	return path.join(MOZ_PATH, profile_path[0])

def get_and_save_icon(app_id, icon_url):
	if not os.path.exists(ICON_PATH):
		os.makedirs(ICON_PATH)

	response = requests.get(icon_url)
	
	media_type = response.headers['content-type']
	ext = ''
	if media_type == 'image/x-icon': ext = '.ico'
	elif media_type == 'image/jpeg': ext = '.jpg'
	elif media_type == 'image/png': ext = '.png'
	else: raise Exception(f"Error getting icon file: {icon_url}")

	icon_path = path.join(ICON_PATH, app_id + ext)
	with open(icon_path, 'wb') as file:
		file.write(response.content)

	return icon_path

def validate_url(url):
	parsed = urllib.parse.urlparse(url)
	return parsed[0] and parsed[1]

def set_ff_config(app_config):
	profile_path = get_ff_profile_path(app_config)

	with open(path.join(profile_path, "user.js"), 'w') as file:
		file.write(get_js(app_config.show_titlebar))

	os.mkdir(path.join(profile_path, "chrome"))
	with open(path.join(profile_path, "chrome", "userChrome.css"), 'w') as file:
		file.write(get_css(app_config.css_config))

def create_desktop_file(app_config, icon_path):
	profile_path = get_ff_profile_path(app_config)

	with open(path.join(APP_PATH, app_config.id + ".desktop"), 'w') as file:
		file.write(f"""\
#!/usr/bin/env xdg-open
[Desktop Entry]
# ffapp-id={app_config.id}
Name={app_config.name}
Exec=firefox --profile {profile_path} --name={app_config.id} --class={app_config.id} {app_config.url} %u
Terminal=false
Type=Application
Icon={icon_path}
Keywords={app_config.name}
StartupWMClass={app_config.id}\n""")

def create_app():
	app_config = get_app_config()
	
	print()
	print(f"Id: {app_config.id}")
	print(f"Name: {app_config.name}")
	print(f"Icon URL: {app_config.icon_url}")
	print(f"CSS: {app_config.css_config.css_option}")
	print(f"Mobile Device: {app_config.css_config.is_mobile}")
	print(f"Show Titlebar: {app_config.show_titlebar}")
	confirm = input("\nPress 'y' to confirm, or any other key to exit: ")
	if confirm != 'y':
		return

	create_ff_profile(app_config.id)
	set_ff_config(app_config)
	create_desktop_file(app_config, get_and_save_icon(app_config.id, app_config.icon_url))

	print(f"\nCreated FF app: {app_config.name}\n")

def update_app():
	app_id = select_app("Update which app?")
	if not app_id: return
	name = None
	icon_url = None
	css_option = None
	is_mobile = None
	show_titlebar = None
	desktop_file_path = path.join(APP_PATH, app_id + ".desktop")
	css_file_path = path.join(MOZ_PATH, app_id, "chrome", "userChrome.css")
	js_file_path = path.join(MOZ_PATH, app_id, "user.js")

	with open(desktop_file_path, 'r') as file:
		lines = file.readlines()
		for line in lines:
			if "Name=" in line:
				name = line.split("=")[-1].rstrip()
				break
		if not name:
			raise Exception(f"Error reading desktop file: {desktop_file_path}")

	print()

	update_name = True if get_and_validate_input(f"Update name ({name})? (y/n): ", ['y', 'n'], "Please choose y/n only") == 'y' else False
	if update_name: name = get_name(name)

	update_icon = True if get_and_validate_input("Update icon? (y/n): ", ['y', 'n'], "Please choose y/n only") == 'y' else False
	if update_icon: icon_url = get_icon_url(None)

	update_css = True if get_and_validate_input("Update browser CSS preferences? (y/n): ", ['y', 'n'], "Please choose y/n only") == 'y' else False
	if update_css: css_option = get_css_option()
	
	update_is_mobile = True if update_css and get_and_validate_input("Update mobile CSS config? (y/n): ", ['y', 'n'], "Please choose y/n only") == 'y' else False
	if update_is_mobile: is_mobile = get_is_mobile()

	update_show_titlebar = True if get_and_validate_input("Update titlebar show/hide? (y/n): ", ['y', 'n'], "Please choose y/n only") == 'y' else False
	if update_show_titlebar: show_titlebar = get_show_titlebar()
	
	print()
	print(f"Id: {app_id}")
	print(f"Name: {name}")
	if update_icon: print(f"Icon URL: {icon_url}")
	if update_css: print(f"CSS: {css_option}")
	if update_css and update_is_mobile: print(f"Mobile Device: {is_mobile}")
	if update_show_titlebar: print(f"Show Titlebar: {show_titlebar}")
	confirm = input("\nPress 'y' to confirm, or any other key to exit: ")
	if confirm != 'y':
		return
	
	if update_name or update_icon:
		with open(desktop_file_path, 'r+') as file:
			new_lines = []
			lines = file.readlines()
			for line in lines:
				if update_name and "Name=" in line:
					line = f"Name={name}\n"
				if update_icon and "Icon=" in line:
					line = f"Icon={get_and_save_icon(app_id, icon_url)}\n"
				new_lines.append(line)
			overwrite_file(file, new_lines)

	if update_css:
		with open(css_file_path, 'w') as file:
			overwrite_file(file, get_css(CssConfig(css_option, is_mobile)).split('\n'))

	if update_show_titlebar:
		with open(js_file_path, 'w') as file:
			overwrite_file(file, get_js(show_titlebar).split('\n'))

	print(f"\nUpdated FF app: {name}\n")

def get_name(name):
	in_name = input(f"Enter app name ({name}): ") or None
	return in_name or name

def get_icon_url(icon_url):
	while True:
		in_icon_url = input(f"Enter icon URL{' (' + icon_url + ')' if icon_url else ''}: ") or None
		icon_url = in_icon_url or icon_url
		if icon_url and validate_url(icon_url):
			break
		else:
			print("Invalid URL")
	
	return icon_url

def get_css_option():
	print("Please choose CSS preferences:")
	print("1) No browser chrome")
	print("2) Back and Forward buttons only")
	print("3) No CSS modifications")
	return get_and_validate_input("", ['1', '2', '3'], "Please choose 1-3")

def get_show_titlebar():
	print("Use system-native Title Bar? (Hiding browser chrome will hide the titlebar and thus the titlebar min/max/close buttons, too.)")
	show_titlebar = get_and_validate_input("Press 'y' for SSD/Native titlebar, and 'n' for default CSD Firefox titlebar: ", ['y', 'n'], "Please choose y/n only")
	return True if show_titlebar == 'y' else False

def get_is_mobile():
	print("Is this being used on a mobile Linux device?")
	is_mobile = get_and_validate_input("Press 'y' to apply mobile-config-firefox CSS, or 'n' for no: ", ['y', 'n'], "Please choose y/n only")
	return True if is_mobile == 'y' else False

def get_app_config():
	url = input("Enter app URL: ")
	if not validate_url(url):
		raise Exception("Invalid URL")

	response = requests.get(url)

	if not str(response.status_code).startswith('2'):
		raise Exception(f"Error reaching: {url}")
	
	html = response.text

	temp = re.search('(?<=<title>).+?(?=</title>)', html, re.DOTALL)
	name = "" if temp is None else temp.group().strip()
	name = get_name(name)

	temp = re.search('(?<=<link rel="icon" href=").+?(?=")', html, re.DOTALL)
	temp = temp if temp else re.search('(?<=<link rel="shortcut icon" href=").+?(?=")', html, re.DOTALL)
	icon_url = "" if temp is None else temp.group().strip()
	if icon_url and not icon_url.startswith("http"):
		icon_url = urllib.parse.urljoin(url, icon_url)
	icon_url = get_icon_url(icon_url)

	css_option = get_css_option()
	is_mobile = get_is_mobile()
	show_titlebar = get_show_titlebar()
		
	return AppConfig(url, name, icon_url, css_option, is_mobile, show_titlebar)

def get_ffapp_filenames():
	filenames = [x for x in os.listdir(APP_PATH) if path.isfile(path.join(APP_PATH, x)) if x.startswith("ffapp-") and x.endswith(".desktop")]
	return [path.join(APP_PATH, x) for x in filenames]

def get_and_validate_input(prompt, valid_list, error_message):
	value = None
	while True:
		value = input(prompt)
		if value not in valid_list:
			print(error_message)
		else:
			break
	return value

def get_ffapp_id_and_name(filename):
	id = None
	name = None
	with open(filename, "r") as file:
		while True:
			line = file.readline()
			if not line:
				break

			if line.startswith("# ffapp-id="):
				id = line[11:].rstrip()
			if line.startswith("Name="):
				name = line[5:].rstrip()

			if id and name:
				return (id, name)
	raise Exception(f"Invalid .desktop file: {filename}")

def select_app(message):
	filenames = get_ffapp_filenames()
	if not filenames or len(filenames) == 0:
		print("No FF apps installed\n")
		return None

	app_ids_and_names = [get_ffapp_id_and_name(x) for x in filenames]
	print(message)
	for i, x in enumerate(app_ids_and_names):
		print(f"  {i + 1}) {x[1]}")

	valid_inputs = [str(x) for x in range(1, len(app_ids_and_names) + 1)]
	delete_index = int(get_and_validate_input("", valid_inputs, "Please select 1-" + str(len(app_ids_and_names))))

	return app_ids_and_names[delete_index - 1][0]

def ff_profile_manager():
	print("Starting Firefox Profile Manager...")
	subprocess.run(["firefox", "--ProfileManager"])
	print()

def list_apps():
	filenames = get_ffapp_filenames()
	if not filenames or len(filenames) == 0:
		print("No FF apps installed\n")
		return

	app_ids_and_names = [get_ffapp_id_and_name(x) for x in filenames]
	print("Currently installed apps:")
	for i, x in enumerate(app_ids_and_names):
		print(f"  {i + 1}) {x[1]}")
	print()

def delete_app():
	app_id = select_app("Delete which app?")
	if not app_id: return

	delete_app_desktop_file(app_id)
	delete_app_icon(app_id)
	delete_ff_profile(app_id)

	print(f"\nDeleted app: {app_id}\n")

def delete_app_desktop_file(app_id):
	desktop_file_path = path.join(APP_PATH, app_id + ".desktop")
	if path.isfile(desktop_file_path):
		os.remove(desktop_file_path)
	else:
		print(f"Error finding desktop file in {APP_PATH}")

def get_icon_path(app_id):
	icon_path = glob.glob(path.join(ICON_PATH, app_id + "*"))  
	if icon_path and len(icon_path) == 1: return icon_path[0]	
	else: return None

def delete_app_icon(app_id):
	icon_path = get_icon_path(app_id)
	if icon_path:
		os.remove(icon_path[0])
	else:
		print(f"Error finding icon in {ICON_PATH}")

def delete_ff_profile(app_id):
	profiles_ini_path = path.join(MOZ_PATH, "profiles.ini")
	with open(profiles_ini_path, 'r+') as file:
		delete_starting_at = None
		lines = file.readlines()
		for i, line in enumerate(lines):
			if f"Name={app_id}" in line:
				delete_starting_at = i - 1
				break
		if delete_starting_at:
			lines = lines[:delete_starting_at - 1] + lines[delete_starting_at + 4:]
			overwrite_file(file, lines)	
		else:
			print(f"Error finding profile {app_id} in {profiles_ini_path}")
	
	ff_profile_path = path.join(MOZ_PATH, app_id)
	if path.isdir(ff_profile_path):
		shutil.rmtree(ff_profile_path)
	else:
		print(f"Error finding profile folder in {MOZ_PATH}")

def overwrite_file(file, lines):
	file.seek(0)
	file.writelines(lines)
	file.truncate()

def main():
	print("  __   __        _ __  _ __ ")
	print(" / _| / _| __ _ | '_ \\| '_ \\")
	print("|  _||  _|/ _` || .__/| .__/")
	print("|_|  |_|  \\__/_||_|   |_|   ")
	print("----------------------------")

	while True:
		print("1) Create an App")
		print("2) List Apps")
		print("3) Update an App")
		print("4) Delete an App")
		print("5) Launch FF Profile Manager")
		print("q) Quit\n")

		option = get_and_validate_input("Please choose 1-5, or press q or Ctrl+C to quit\n", ['1', '2', '3', '4', '5', 'q'], "Please choose 1-5, or press q or Ctrl+C to quit")

		print()

		if option == '1': create_app()
		elif option == '2': list_apps()
		elif option == '3': update_app()
		elif option == '4': delete_app()
		elif option == '5': ff_profile_manager()
		elif option == 'q': break

main()
